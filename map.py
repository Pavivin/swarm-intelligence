from util import Direction
from collections import defaultdict
from ant import Ant
from food import Food
from nest import Nest

class Map:

    prohibited_positions = [0] * len(Direction)

    positions_of_pheromones = [0] * len(Direction)

    pheromones = defaultdict(dict)

    @staticmethod
    def is_outside_border(point):
        if(point[0] <= 0 or point[0]+Ant.width >= Map.width or
           point[1] <= 0 or point[1]+Ant.height >= Map.height):
            return True
        else:
            return False

    @classmethod
    def decide_prohibited_positions(cls, positions):
        for d, pos in enumerate(positions):
            cls.prohibited_positions[d] = 1 if(
                Map._is_allowed_to_pass(pos)) else 0

    @classmethod
    def decide_pheromones_of_positions(cls, positions):
        for d, pos in enumerate(positions):
            pher = 0
            for leader in cls.pheromones[pos]:
                pher += cls.pheromones[pos][leader]

            cls.positions_of_pheromones[d] = pher

    @classmethod
    def add_pheromones(cls, position, leader_id, amount):

        if leader_id not in cls.pheromones[position]:
            cls.pheromones[position] = {leader_id: amount}
        else:
            cls.pheromones[position][leader_id] += amount

    @classmethod
    def evaporate_pheromones(cls, evaporation_rate):
        for pos in cls.pheromones:
            for leader in cls.pheromones[pos]:
                cls.pheromones[pos][leader] *= (1-evaporation_rate)
                if cls.pheromones[pos][leader] < 0.01:
                    cls.pheromones[pos][leader] = 0

    @staticmethod
    def get_leader_of_food_trace(position):
        for leader in Map.pheromones[position]:
            if Map.pheromones[position][leader] > 0:
                return leader
        else:
            return False

    @staticmethod
    def _is_allowed_to_pass(pos):
        if (Food.collided_with_empty_food(pos) or
           Nest.collided(pos, enlarged_scope=Ant.width) or
           Map.is_outside_border(pos)):
            return False
        return True