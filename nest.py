import pygame

class Nest:
    rect = None
    food_amount = 0

    @classmethod
    def set_image(cls, image, size):
        cls.size = size
        cls.image = pygame.transform.scale(image, size)

    @classmethod
    def set_position(cls, position):
        cls.position = position
        cls.rect = cls.image.get_rect(topleft=position)

    @staticmethod
    def collided(pos, enlarged_scope):
        rect = pygame.Rect(Nest.rect.left-enlarged_scope, Nest.rect.top-enlarged_scope,
                           Nest.rect.width+2*enlarged_scope, Nest.rect.height+2*enlarged_scope)
        if(rect.collidepoint(pos)):
            return True
        return False