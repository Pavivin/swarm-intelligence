import os
import pygame
import random
from util import Direction, Role, Action

class Ant():

    max_steps = 5
    default_probability_of_direction = {}
    images = {}

    @classmethod
    def construct_proba_of_direction(cls):
        length = len(Direction)
        for item in Direction:
            proba = [0]*length
            proba[item.value] = 8
            proba[item.value-1] = 1
            proba[item.value+1 if item.value < length-1 else 0] = 1
            proba[item.value-2] = 0.01
            proba[item.value+2 if item.value < length -
                  2 else (item.value+1) % 7] = 0.01
            cls.default_probability_of_direction[item.name] = proba
        # print(cls.default_probability_of_direction)

    @classmethod
    def set_image(cls, folder):
        filesname = os.listdir(folder)
        for file in filesname:
            direction = file.split(".")[0]
            image = pygame.image.load(f"{folder}/{file}")
            Ant.images[direction] = pygame.transform.scale(
                image, (cls.width, cls.height))

    @staticmethod
    def _choice(p):
        rand = random.random()*sum(p)
        acc = 0
        for i, ele in enumerate(p):
            acc += ele
            if(acc >= rand):
                return i

    def __init__(self, _id, start_pos, direction):
        self._id = _id
        self.role = Role.FORAGER
        self.action = Action.FORAGE
        self.drop_amount = 2
        self.direction = direction
        self.position = start_pos
        self.peripheral_positions = [""]*len(Direction)

        self.path = [""]*(Ant.max_steps+1)
        self.path[0] = (start_pos, direction)
        self.current_proba_directions = Ant.default_probability_of_direction[direction]

    def follow_leader(self, leader):
        self.leader_id = leader._id
        self._action = Action.GO
        self.destination_step = leader.destination_step
        self.food = leader.food

        # change to leader route
        for i, path in enumerate(leader.path):
            self.path[i] = path

            # set current step
            if path != "" and path[0] == self.position:
                self.current_step = i
                break
        else:
            self.current_step = 1

    def has_reached_maximum_step(self):
        return self.current_step == self.max_steps+1

    def has_arrived_at_nest(self):
        return self.current_step == -1

    def has_arrived_at_food(self):
        return self.current_step == self.destination_step

    @property
    def action(self):
        return self._action

    @action.setter
    def action(self, value):
        if(value == Action.FORAGE):
            self.current_step = 1

        elif(value == Action.RETURN):
            self.current_step -= 1

        elif(value == Action.TRANSPORT):
            self.destination_step = self.current_step  # food
            self.current_step -= 1

        elif(value == Action.GO):
            self.current_step = 1

        self._action = value

    @property
    def direction(self):
        return self._direction

    @direction.setter
    def direction(self, direction):
        self._direction = direction
        self.image = Ant.images[self.direction]

    @property
    def position(self):
        return self._position

    @position.setter
    def position(self, position):
        self._position = position
        self.rect = self.image.get_rect(topleft=self._position)

    def forager_move(self, prohibited_positions, positions_of_pheromones):
        # 1.往周圍八格費洛蒙濃度最高的方向移動 or
        # 2.若沒有費洛蒙則以預設機率決定方向

        highest_concentration = max(positions_of_pheromones)
        if(highest_concentration > 0):
            index = positions_of_pheromones.index(highest_concentration)
        else:
            # if position is prohibited , probability is 0
            for i in range(len(Direction)):
                self.current_proba_directions[i] = Ant.default_probability_of_direction[self.direction][i] * \
                    prohibited_positions[i]
            index = Ant._choice(p=self.current_proba_directions)

        self.direction = Direction(index).name
        self.position = self.peripheral_positions[index]

        self.path[self.current_step] = (self.position, self.direction)
        self.current_step += 1

    def reverse_move(self):
        pos, direction = self.path[self.current_step]
        # reversed direction
        self.direction = Direction(
            (Direction[direction].value+4) % len(Direction)).name
        self.position = pos
        self.current_step -= 1

    def move_to_food(self):
        # carrier move to find food along with same way
        pos, direction = self.path[self.current_step]
        self.direction = direction
        self.position = pos
        self.current_step += 1

    def perceive_peripheral_positions(self):
        # east
        left = self.position[0]+Ant.width
        top = self.position[1]
        self.peripheral_positions[Direction.EAST.value] = (left, top)

        # south east
        left = self.position[0]+Ant.width
        top = self.position[1]+Ant.height
        self.peripheral_positions[Direction.SOUTHEAST.value] = (left, top)

        # south
        left = self.position[0]
        top = self.position[1]+Ant.height
        self.peripheral_positions[Direction.SOUTH.value] = (left, top)

        # south west
        left = self.position[0]-Ant.width
        top = self.position[1]+Ant.height
        self.peripheral_positions[Direction.SOUTHWEST.value] = (left, top)

        # west
        left = self.position[0]-Ant.width
        top = self.position[1]
        self.peripheral_positions[Direction.WEST.value] = (left, top)

        # north west
        left = self.position[0]-Ant.width
        top = self.position[1]-Ant.height
        self.peripheral_positions[Direction.NORTHWEST.value] = (left, top)

        # NORTH
        left = self.position[0]
        top = self.position[1]-Ant.height
        self.peripheral_positions[Direction.NORTH.value] = (left, top)

        # NORTH east
        left = self.position[0]+Ant.width
        top = self.position[1]-Ant.height
        self.peripheral_positions[Direction.NORTHEAST.value] = (left, top)

        return self.peripheral_positions