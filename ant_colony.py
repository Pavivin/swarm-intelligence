from collections import defaultdict

class AntColony(list):

    def __init__(self):
        self.pheromones = defaultdict(int)

    def add_pheromones(self, ant_id, amount):
        self.pheromones[ant_id] += amount

    def add_ant(self, ant):
        self.append(ant)

    def remove_all_ant(self):
        self.clear()

    def evaporate_pheromones(self, evaporation_rate):
        for ant in self.pheromones:
            self.pheromones[ant] *= (1-evaporation_rate)
            if (self.pheromones[ant] < 0.001):
                self.pheromones[ant] = 0
