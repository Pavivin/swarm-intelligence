import sys
import pygame
import random
from util import Button, Direction, Color, Role, Action
import time


from food import Food
from nest import Nest
from ant_colony import AntColony
from ant import Ant
from map import Map


DEBUG = False

Food.width = 50
Food.height = 50
print('Введите количество частичек еды')
Food.count = int(input()) + 1

Nest.width = 70
Nest.height = 70

ANT_COLONY = AntColony()

Ant.width = 15
Ant.height = 15
print('Введите количество муравьёв')
Ant.count = int(input())
Ant.max_steps = 100

# screen
DEFAULT_BG_COLOR = Color.WHITE

Map.width = 700
Map.height = 700
SCREEN = None
SCREEN_WIDTH = Map.width
SCREEN_HEIGHT = Map.height + 100

EVAPORATION_RATE = 0.01

# action
start_btn = None
IS_START = False
CURRENT_TIME = 0

maps = []
for w in range(int(Map.width / Food.width)):
    for h in range(int(Map.height / Food.height)):
        maps.append((w*Food.width, h * Food.height))

# resources
random_map_img = pygame.image.load('image/random_map.png')
start_img = pygame.image.load("image/start.png")
stop_img = pygame.image.load("image/stop.png")

nest_img = pygame.image.load("image/nest.png")
food_img = pygame.image.load("image/fruit.png")

Ant.set_image("image/ant2")
Ant.construct_proba_of_direction()

Nest.set_image(nest_img, (Nest.width, Nest.height))

Food.set_image(food_img, (Food.width, Food.height))


def random_generate_map():
    if (IS_START):
        return

    clear_map()
    # clear pheromones
    Map.pheromones.clear()

    possible_positions = random.sample(maps, Food.count)

    # nest
    Nest.set_position(possible_positions.pop())
    SCREEN.blit(Nest.image, Nest.rect)

    # food
    for pos in possible_positions:
        food = Food(pos)
        Food.add_food(food)
        SCREEN.blit(Food.image, food.rect)

    # ant
    left = Nest.position[0]-2*Ant.height
    top = Nest.position[1]-2*Ant.width
    right = Nest.position[0]+Nest.width+Ant.height
    bottom = Nest.position[1]+Nest.height+Ant.height

    possible_positions = [(w, top) for w in range(left, right, Ant.width)]
    possible_positions.extend([(w, bottom)
                              for w in range(left, right, Ant.width)])
    possible_positions.extend([(left, h)
                              for h in range(top, bottom, Ant.height)])
    possible_positions.extend([(right, h)
                              for h in range(top, bottom, Ant.height)])

    possible_positions = [
        pos for pos
        in set(possible_positions)
        if (Map.is_outside_border(pos) + Map.is_outside_border((pos[0], pos[1] + Ant.height)) == 0)
    ]

    sample_count = min(len(possible_positions), Ant.count)
    ants_positions = random.sample(possible_positions, sample_count)

    for i, pos in enumerate(ants_positions):
        ant = Ant(i, pos, Direction.NORTH.name)
        ANT_COLONY.add_ant(ant)
        SCREEN.blit(ant.image, ant.rect)


def clear_map():
    map_rect = pygame.Rect(0, 0, Map.width, Map.height)
    pygame.draw.rect(SCREEN, DEFAULT_BG_COLOR, map_rect)

    ANT_COLONY.remove_all_ant()
    Food.remove_all_food()


def start():
    global IS_START, CURRENT_TIME, start_btn

    if(IS_START):
        IS_START = False
        start_btn.change_image(start_img)

    else:
        IS_START = True
        CURRENT_TIME = time.time()
        start_btn.change_image(stop_img)

    SCREEN.blit(start_btn.image, start_btn.rect)


def main():
    global SCREEN, IS_START, CURRENT_TIME, start_btn

    pygame.init()
    pygame.font.init()
    myfont = pygame.font.SysFont('Comic Sans MS', 25)

    SCREEN = pygame.display.set_mode((Map.width, Map.height + 100))
    SCREEN.fill(DEFAULT_BG_COLOR)

    random_generate_map()

    pygame.draw.line(SCREEN, Color.SLATE_GREY,
                     (0, Map.height), (SCREEN_WIDTH, Map.height))

    random_map = Button((0, Map.height), (80, 50),
                        random_map_img, random_generate_map)
    SCREEN.blit(random_map.image, random_map.rect)

    # start
    start_btn = Button((random_map.position[0] + random_map.size[0], Map.height),
                       (80, 50), start_img, start)
    SCREEN.blit(start_btn.image, start_btn.rect)

    running = True

    while running:
        end = time.time()

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()

            if event.type == pygame.MOUSEBUTTONDOWN:
                random_map.on_click(event)
                start_btn.on_click(event)

        if (IS_START and (end-CURRENT_TIME > 0.5)):
            for ant in ANT_COLONY:
                # overwrite original image
                pygame.draw.rect(SCREEN, DEFAULT_BG_COLOR, ant.rect)
                if(ant.role == Role.FORAGER):
                    if(ant.action == Action.FORAGE):

                        # Move in the direction of the highest concentration
                        # of eight pheromones around.
                        # If there is no pheromone, move randomly
                        ant.perceive_peripheral_positions()
                        Map.decide_pheromones_of_positions(
                            ant.peripheral_positions)
                        Map.decide_prohibited_positions(
                            ant.peripheral_positions)
                        ant.forager_move(Map.prohibited_positions,
                                         Map.positions_of_pheromones)

                        food = Food.collided_with_food(
                            pos=ant.position, enlarged_scope=2*Ant.width)
                        leader_id = Map.get_leader_of_food_trace(ant.position)

                        if (food):
                            # Ant.leaders.append(ant._id)
                            ant.role = Role.LEADER
                            ant.action = Action.TRANSPORT
                            ant.food = food
                            ant.food.size -= 1

                        elif (leader_id):
                            # follow path
                            ant.role = Role.FOLLOWER
                            ant.follow_leader(ANT_COLONY[leader_id])

                        elif (ant.has_reached_maximum_step()):
                            ant.action = Action.RETURN
                            # print(f"path:\n{ant.path}")

                    elif (ant.action == Action.RETURN):
                        pygame.draw.rect(SCREEN, DEFAULT_BG_COLOR, ant.rect)
                        ant.reverse_move()

                        leader_id = Map.get_leader_of_food_trace(ant.position)

                        if (leader_id):
                            # follow path
                            ant.role = Role.FOLLOWER
                            ant.follow_leader(ANT_COLONY[leader_id])

                        elif (ant.has_arrived_at_nest()):
                            ant.action = Action.FORAGE

                elif (ant.role == Role.LEADER):
                    if (ant.action == Action.GO):
                        Map.add_pheromones(
                            ant.position, ant._id, ant.drop_amount)
                        ANT_COLONY.add_pheromones(ant._id, ant.drop_amount)
                        ant.move_to_food()

                        if (ant.has_arrived_at_food()):
                            if (ant.food.is_empty()):
                                ant.role = Role.FORAGER
                                ant.action == Action.FORAGE
                            else:
                                ant.food.size -= 1
                                ant.action = Action.TRANSPORT

                    elif(ant.action == Action.TRANSPORT):
                        Map.add_pheromones(
                            ant.position, ant._id, ant.drop_amount)
                        ANT_COLONY.add_pheromones(ant._id, ant.drop_amount)
                        ant.reverse_move()

                        if (ant.has_arrived_at_nest()):
                            Nest.food_amount += 1
                            ant.action = Action.GO

                elif (ant.role == Role.FOLLOWER):
                    if (ant.action == Action.TRANSPORT):
                        Map.add_pheromones(
                            ant.position, ant._id, ant.drop_amount)
                        ANT_COLONY.add_pheromones(
                            ant.leader_id, ant.drop_amount)
                        ant.reverse_move()

                        if (ant.has_arrived_at_nest()):
                            Nest.food_amount += 1
                            ant.action = Action.GO

                            # evaluate different route for same food
                            better_id = ant.leader_id
                            for leader_id in ANT_COLONY.pheromones:
                                if leader_id == ant.leader_id:
                                    continue
                                if(ANT_COLONY.pheromones[leader_id] > ANT_COLONY.pheromones[better_id]):
                                    better_id = leader_id

                            if(ant.leader_id != better_id):
                                # switch to better route
                                ant.follow_leader(ANT_COLONY[better_id])

                    elif (ant.action == Action.GO):
                        Map.add_pheromones(
                            ant.position, ant._id, ant.drop_amount)
                        ANT_COLONY.add_pheromones(
                            ant.leader_id, ant.drop_amount)
                        pygame.draw.rect(SCREEN, Color.TRACE_2, ant.rect)
                        ant.move_to_food()

                        if(ant.has_arrived_at_food()):
                            if(ant.food.is_empty()):
                                ant.role = Role.FORAGER
                                ant.action == Action.FORAGE
                            else:
                                ant.food.size -= 1
                                ant.action = Action.TRANSPORT

            # redraw map
            for pos in Map.pheromones:
                pher = 0
                for leader in Map.pheromones[pos]:
                    pher += Map.pheromones[pos][leader]

                rect = pygame.Rect(pos[0], pos[1], Ant.width, Ant.height)

                if pher > 15:
                    pygame.draw.rect(SCREEN, Color.TRACE_5, rect)
                elif(pher > 10):
                    pygame.draw.rect(SCREEN, Color.TRACE_4, rect)
                elif(pher > 5):
                    pygame.draw.rect(SCREEN, Color.TRACE_3, rect)
                elif(pher > 0):
                    pygame.draw.rect(SCREEN, Color.TRACE_2, rect)
                else:
                    pygame.draw.rect(SCREEN, DEFAULT_BG_COLOR, rect)

            # redraw ant
            for ant in ANT_COLONY:
                SCREEN.blit(ant.image, ant.rect)

            # evaporate
            Map.evaporate_pheromones(EVAPORATION_RATE)
            ANT_COLONY.evaporate_pheromones(EVAPORATION_RATE)

            CURRENT_TIME = end
        pygame.display.update()


if __name__ == "__main__":
    main()
