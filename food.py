import pygame

class Food():

    all_food = []

    @classmethod
    def add_food(cls, food):
        cls.all_food.append(food)

    @classmethod
    def remove_all_food(cls):
        cls.all_food.clear()

    @classmethod
    def set_image(cls, image, size):
        cls.image = pygame.transform.scale(image, size)

    @staticmethod
    def collided_with_empty_food(pos):
        food = Food.collided_with_food(pos)
        if(food and food.size == 0):
            return True
        else:
            return False

    @staticmethod
    def collided_with_food(pos, enlarged_scope=None):
        for food in Food.all_food:
            rect = food.rect
            if enlarged_scope:
                rect = pygame.Rect(food.rect.left-enlarged_scope, food.rect.top-enlarged_scope,
                                   food.rect.width+2*enlarged_scope, food.rect.height+2*enlarged_scope)

            if(rect.collidepoint(pos)):
                return food
        else:
            return None

    def __init__(self, position):
        self.position = position
        self.rect = self.image.get_rect(topleft=position)
        self.size = 150

    def is_empty(self):
        return self.size == 0